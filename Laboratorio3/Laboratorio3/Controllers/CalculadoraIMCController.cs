﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Laboratorio3.Models;

namespace Laboratorio3.Controllers
{
    public class CalculadoraIMCController : Controller
    {
        public ActionResult ResultadoIMC() {
            PersonaModel persona = new PersonaModel(1, "Cristiano Ronaldo", 84.0, 1.87);
            double IMC = persona.Peso / (persona.Estatura * persona.Estatura);
            ViewBag.IMC = IMC;
            ViewBag.persona = persona;
            return View();
        }

        public ActionResult ResultadosAleatoriosIMC() {
            Random random  = new Random();
            double[] vector = new double[30];
            List<PersonaModel> listaPersonas = new List<PersonaModel>();
            int Id = 0;
            for(int i = 0; i < 30; ++i)
            {
                PersonaModel persona = new PersonaModel(Id, "Sin nombre", random.NextDouble() * (150.0 - 20.0) + 20.0, random.NextDouble() * (2.0 - 1.0) + 1.0);
                double IMC = persona.Peso / (persona.Estatura * persona.Estatura);
                vector[Id] = IMC;
                listaPersonas.Add(persona);
                Id++;
            }

            ViewBag.listaPersonas = listaPersonas;
            ViewBag.vectorIMC = vector;
            return View();
        }
        // GET: CalculadoraIMC
        public ActionResult Index()
        {
            return View();
        }
    }
}